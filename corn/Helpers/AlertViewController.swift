//
//  AlertViewController.swift
//  SmartCity
//
//  Created by Ioannis Silvestridis on 22/2/21.
//

import UIKit

protocol AlertReturn{
    func returnOK()

}

class AlertViewController: UIViewController {
    var alertDelegate :AlertReturn? = nil
    @IBOutlet var okButn: UIButton!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var messageDetailsLabel: UILabel!
    @IBOutlet var messageLAbel: UILabel!
    
    var message :String?
    var messageDetails :String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        if message == "Success" || message == "Attention" {
            cancelBtn.isHidden = true
        }
    }
    func setUpView(){
        
        messageLAbel.text = message
        messageDetailsLabel.text = messageDetails
    }
    @IBAction func cancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        alertDelegate?.returnOK()
        
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
