//
//  PopUp.swift
//  SmartCity
//
//  Created by Ioannis Silvestridis on 3/2/21.
//

import Foundation
import UIKit
private let sharedPopUp = PopUp()

class PopUp: UIView{

    class var sharedInstance: PopUp {
        
        return sharedPopUp
    }
    
    func showSimpleAlertPopUpWith(title:String, message:String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        
        alertController.addAction(okAction)
      
       let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(alertController, animated: true, completion: nil)
        // topController should now be your topmost view controller
        }
       
  
    }
        
        
    
}
