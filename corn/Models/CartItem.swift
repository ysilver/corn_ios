//
//  CartItem.swift
//  corn
//
//  Created by Ioannis Silvestridis on 11/6/21.
//

import Foundation

class CartItem {
    
    var id : Int
    var comments : String
    var qnt : String
    var itemDesc: String
    var price: String
    var imageName:String
    
    
    init(qnt:String ,itemDesc:String,price:String,imageName:String,id:Int,comments:String){
        
        self.qnt = qnt
        self.price = price
        self.itemDesc = itemDesc
        self.imageName = imageName
        self.comments =  comments
        self.id = id
        
       
    }
}
