//
//  Order.swift
//  corn
//
//  Created by Ioannis Silvestridis on 24/6/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let order = try? newJSONDecoder().decode(Order.self, from: jsonData)

import Foundation

// MARK: - Order
struct Order {
    let address, comments: String
    let items: [Item]
}

// MARK: - Item
struct Item{
    let productID, quantity: Int
    let comments: String


}
