//
//  CommentsViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 17/6/21.
//

import UIKit
import Localize_Swift
import SwiftSpinner
class CommentsViewController: UIViewController, ReturnOrderDelegate, AlertReturn{
    func returnOK() {
        Cart.sharedInstance.items = []
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func returnOrder() {
        SwiftSpinner.hide(nil)
        let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
        
        alertViewController.message = "Success".localized()
        alertViewController.messageDetails = "We rerceived your order and its on its way"
        alertViewController.alertDelegate = self
       // alertViewController.cancelBtn.isHidden = true
        self.present(alertViewController, animated: true, completion: nil)
       
    }
    
    var address: String?
    @IBOutlet var commentsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Comments"
       // address = "τεπελενιου 15, 15452, π. ψυχικο"
        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func callOnDeliveryAction(_ sender: Any) {
    }
    
    @IBAction func placeOrder(_ sender: Any) {
        
        SwiftSpinner.show("Connecting")
        var items = [[String:Any]]()
       
        for cartItem in Cart.sharedInstance.items! {
            
            
            let qnt = cartItem.qnt
            let comments = cartItem.comments
            let orderItem = ["product_id":cartItem.id,
                             "quantity":Int("\(qnt)")!,
                             "comments":"\(comments)"] as [String : Any]
            
            items.append(orderItem)
            
            }
        let parameters:[String: Any] = ["address":"\(address!)",
                          "comments":"\(commentsTextView!.text ?? "no comments")",
                          "items":items] as [String : Any]
//
        
        print("items are:\(items)")
        
        NetworkOperations.sharedInstance.returnDelegate = self
        NetworkOperations.sharedInstance.placeOrder(parameters: parameters)
    }
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
