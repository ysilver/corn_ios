//
//  ViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 8/6/21.
//

import UIKit
import GoogleMaps
import GooglePlaces
//import Localize_Swift


class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet var rImage: UIImageView!
    
    @IBOutlet var threeItemsBtn: UIButton!
    @IBOutlet var fourItemsBtn: UIButton!
    //@IBOutlet var threeItemsBtn: UIStackView!
    @IBOutlet var addressTextField: UITextField!
    
    @IBOutlet var twoItemsBtn: UIButton!
    @IBOutlet var priceTextField: UITextField!
    @IBOutlet var itemsTextField: UITextField!
    @IBOutlet var typeCookingLbl: UILabel!
    @IBOutlet var roastedButton: UIButton!
    @IBOutlet var boiledButton: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var bImage: UIImageView!
    var product_id : Int?
    @IBOutlet var logoView: UIView!
    var location:CLLocation?
    var locationManager = CLLocationManager()
    var counter:Int = 0
    var window: UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard

        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            print("App already launched : \(isAppAlreadyLaunchedOnce)")
            let api_token = UserDefaults.standard.value(forKey: "api_token")
            
            if api_token != nil {
                
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)

             let initialViewController = storyboard.instantiateViewController(withIdentifier: "MobileNoViewController")
             initialViewController.modalPresentationStyle = .fullScreen
             self.navigationController?.present(initialViewController, animated: true, completion: nil)
            }
            
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            print("App launched first time")
//            let window = UIWindow(frame: UIScreen.main.bounds)
//            self.window = window
               let storyboard = UIStoryboard(name: "Main", bundle: nil)

            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TermsViewController")
            initialViewController.modalPresentationStyle = .fullScreen
            self.navigationController?.present(initialViewController, animated: true, completion: nil)

              
        }
//      
       // bImage.layer.borderWidth = 1
            bImage.layer.masksToBounds = false
            bImage.layer.borderColor = UIColor.black.cgColor
            bImage.layer.cornerRadius = bImage.frame.height/2
            bImage.clipsToBounds = true
      //  rImage.layer.borderWidth = 1
            rImage.layer.masksToBounds = false
            rImage.layer.borderColor = UIColor.black.cgColor
        rImage.layer.cornerRadius = rImage.frame.height/2
            rImage.clipsToBounds = true
       
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)

        self.tabBarController?.title = "Christina's Corn"
    }

 

    @IBAction func roastedBtnClickd(_ sender: Any) {
        //typeCookingLbl.text = "ΨΗΤΟ"
        product_id =  1
        self.performSegue(withIdentifier: "roastedOrder", sender: self)
    }

    
    @IBAction func boiledButtonClickd(_ sender: Any) {
      //  typeCookingLbl.text = "ΒΡΑΣΤΟ"
        product_id = 2
        self.performSegue(withIdentifier: "boiledOrder", sender: self)
    }

 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
       
        let destViewController =  segue.destination as! OrderViewController
        destViewController.product_id = product_id!
        // Pass the selected object to the new view controller.
    }
               //     addressString = lines[0]
//                self.currentAddress = lines.joined(separator: "\n")
//                self.addressLabel.text = "\(self.currentAddress)
}

