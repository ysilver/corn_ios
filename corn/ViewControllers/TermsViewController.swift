//
//  TermsViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 9/6/21.
//

import UIKit

class TermsViewController: UIViewController {

    @IBOutlet var nextBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.navigationController?.performSegue(withIdentifier: "showMobile", sender: self)
    }
    @IBAction func termsSwitch(_ sender: Any) {
        nextBtn.isEnabled =  true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
