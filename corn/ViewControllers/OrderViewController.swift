//
//  OrderViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 10/6/21.
//

import UIKit
import Localize_Swift
class OrderViewController: UIViewController {

    @IBOutlet var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var comments: UITextField!
    @IBOutlet var price: UILabel!
    @IBOutlet var stepper: UIStepper!
    @IBOutlet var quantity: UITextField!
    @IBOutlet var itemDetails: UILabel!
    @IBOutlet var cornImage: UIImageView!
    @IBOutlet var mainImage: UIImageView!
    var product_id :Int = 0
    var constrainORiginalValue:CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        quantity.text = "2"
        constrainORiginalValue = bottomSpaceConstraint.constant
        if product_id == 2 {
            itemDetails.text = " Boiled corn".localized()
            mainImage.image = UIImage(named: "BCorn")
        }
        else{
            itemDetails.text = "Roasted corn".localized()
            mainImage.image = UIImage(named: "RCorn")
        }
        price.text = "3.99"
        stepper.value  = 2
       
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        quantity.isUserInteractionEnabled = false
        NotificationCenter.default.addObserver(self,
           selector: #selector(self.keyboardNotification(notification:)),
           name: UIResponder.keyboardWillChangeFrameNotification,
           object: nil)
       }
     
       deinit {
         NotificationCenter.default.removeObserver(self)
       }
    @objc func keyboardNotification(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

      
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomSpaceConstraint.constant = keyboardFrame.size.height + 20
        })
        
        
    }

    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomSpaceConstraint.constant = self.constrainORiginalValue!
        })
        
    }
    
    @IBAction func StepperChanged(_ sender: UIStepper) {
        
        quantity.text = Int((sender).value).description
        if quantity.text == "1" {
            price.text = "Not available"
        }
        else if quantity.text == "2" {
            price.text = "3,99"
        }
        else if quantity.text == "3" {
            price.text = "5,99"
            }
        else if quantity.text == "4" {
            price.text = "7,99"
        }
        else{
            price.text = "9,99"
        }
    }
  
    @IBAction func addToBasketnCheckout(_ sender: Any) {
    
        var imageName: String = ""
        if product_id == 1 {
            imageName = "RCorn"
        }
        else {
            imageName = "BCorn"
        }
        let cartItem = CartItem(qnt: quantity.text!, itemDesc: itemDetails.text!, price: price.text!, imageName:imageName,id :product_id, comments :comments.text ?? "")
        
        Cart.sharedInstance.items?.append(cartItem)
        print(Cart.sharedInstance.items?.count ?? 101)
     //   self.performSegue(withIdentifier: "checkoutBasket", sender: self)
    }
    
    @IBAction func continueShopping(_ sender: Any) {
        
        var imageName: String = ""
        if product_id == 1 {
            imageName = "RCorn"
        }
        else {
            imageName = "BCorn"
        }
        let cartItem = CartItem(qnt: quantity.text!, itemDesc: itemDetails.text!, price: price.text!, imageName:imageName,id :product_id, comments :comments?.text ?? "")
      
        
        Cart.sharedInstance.items?.append(cartItem)
        print(Cart.sharedInstance.items?.count ?? 101)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
