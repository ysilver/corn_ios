//
//  SmsViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 21/6/21.
//

import UIKit
import SwiftSpinner
class SmsViewController: UIViewController, ReturnVerifyPhone {
    func returnVerifyPhone() {
        SwiftSpinner.hide(nil)
        UserDefaults.standard.set(DataObject.sharedInstance.user?.apiToken, forKey: "api_token")
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    

    @IBOutlet var fourthChar: UITextField!
    @IBOutlet var thirdChar: UITextField!
    @IBOutlet var secndChar: UITextField!
    @IBOutlet var firstChar: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @IBAction func sendCodeAction(_ sender: Any) {
        let code = firstChar?.text ?? ""
        
        let parameters = ["code":code]
        SwiftSpinner.show("Connecting")
        NetworkOperations.sharedInstance.verifyPhoneDelegate = self
        NetworkOperations.sharedInstance.verifyPhone(parameters: parameters)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
