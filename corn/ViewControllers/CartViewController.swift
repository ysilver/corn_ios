//
//  CartViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 11/6/21.
//

import UIKit

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("items in cart:\( Cart.sharedInstance.items!.count)")
        return Cart.sharedInstance.items!.count  + 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Christina's Corn"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row   == Cart.sharedInstance.items!.count {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartTotalCell") as! CartTotalTableViewCell
            var grandTotal:Double = 0.00
            if let items = Cart.sharedInstance.items {
                
       
                for price in  items{
                
               let itemPrice = price.price
                   let itemDecimal = itemPrice.replacingOccurrences(of: ",", with: ".")
                    grandTotal =  grandTotal  + (Double(itemDecimal) ?? 1.00)
                }
            }
            cell.totalPrice.text  = String(grandTotal)
            return cell
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartItemCell") as! CartItemTableViewCell
            
            cell.itamImageView.image = UIImage(named: "\(String( (Cart.sharedInstance.items?[indexPath.row].imageName) ?? "noname"))")
            cell.itemDescriptionlbl.text  =  Cart.sharedInstance.items?[indexPath.row].itemDesc
            cell.priceLbl.text = Cart.sharedInstance.items?[indexPath.row].price
            cell.quantityLbl.text = Cart.sharedInstance.items?[indexPath.row].qnt
            cell.comments.text = Cart.sharedInstance.items?[indexPath.row].comments
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            Cart.sharedInstance.items?.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "cartItemCell")
        tableView.register(UINib(nibName: "CartTotalTableViewCell", bundle: nil), forCellReuseIdentifier: "cartTotalCell")
        self.title = "My Cart"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.title = "My Cart"
        
        tableView.reloadData()
    }
    @IBAction func sendOrderAction(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
