//
//  AddressViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 14/6/21.
//

import UIKit
import GoogleMaps
class AddressViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, AlertReturn {
    func returnOK() {
        
        self.performSegue(withIdentifier: "showComments", sender: self)
       
    }
    

    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var mapViewHolder: UIView!
    var mapView = GMSMapView()
    var marker  = GMSMarker()
    var counter:Int = 0
    var location:CLLocation?
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    override func viewWillAppear(_ animated: Bool)
        {
            super.viewWillAppear(true)
            self.title = "Address"
            
            let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            self.view.addGestureRecognizer(tap)
            self.locationManager.requestWhenInUseAuthorization()
                 self.locationManager.requestAlwaysAuthorization()
                 self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
               //  self.locationManager.allowsBackgroundLocationUpdates = true
                 self.locationManager.delegate = self
                 self.locationManager.startUpdatingLocation()
            switch CLLocationManager.authorizationStatus() {
                       case .denied:
                           print("No access")
                        PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Attention", message:  "If you want to send your coordinates you need to renable location access in settings")
         
                       case .authorizedAlways, .authorizedWhenInUse:
                           print("Access")
                         self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                       //  self.locationManager.allowsBackgroundLocationUpdates = true
                         self.locationManager.delegate = self
                         self.locationManager.startUpdatingLocation()
                     case  .notDetermined, .restricted:
                         self.locationManager.requestWhenInUseAuthorization()
                         self.locationManager.requestAlwaysAuthorization()
                        PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Attention", message:  "If you want to send your coordinates you need to renable location access in settings")
                       @unknown default:
                       break
            }
            
        }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            counter = counter+1
            location = locations.last
            print(location?.coordinate.latitude ?? 0.0)
            self.locationManager.stopUpdatingLocation()
    
            if counter > 1{}
            else{
                if let location = location {
                    adjustMapAndCamera(location: location)
                }
            }
        }

    func adjustMapAndCamera(location:CLLocation?){
        
        
        
        let camera = GMSCameraPosition.camera(withLatitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude, zoom: 17.0)
       
        self.mapView = GMSMapView.map(withFrame: self.mapViewHolder.bounds, camera: camera)
        self.mapView.camera = camera
        marker.map =  self.mapView
        marker.icon = UIImage(named: "marker")
        marker.position = camera.target
           
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self
        self.mapView.mapType = .hybrid
        self.mapViewHolder.addSubview(self.mapView)
        
         
    }
        
        //MapView delegates
            func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
                print("didBeginDragging")
            }
            func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        //       textField.resignFirstResponder()
        //
        //        locationManager.delegate = nil
                marker.position = position.target
            }
            func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
              
                    reverseGeocoding(marker: marker)
                
            }
        
        func reverseGeocoding(marker: GMSMarker) {
            let geocoder = GMSGeocoder()
            let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
            
            
            geocoder.reverseGeocodeCoordinate(coordinate) { [self] response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    
                    print("Response is = \(address)")
                    print("Response is = \(lines)")
                    addressTextField.text = "\(lines[0])"
                   // addressString = lines[0]
    //                self.currentAddress = lines.joined(separator: "\n")
    //                self.addressLabel.text = "\(self.currentAddress) lat:\(marker.position.latitude) ,lon:\(marker.position.longitude)"
                }
              
            }
        }
        
        
        
        
        
        
    @IBAction func sendOrderAction(_ sender: Any) {
        let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
        
        alertViewController.message = "Are you sure your location is:"
        alertViewController.messageDetails = "\(addressTextField.text ?? "no address")"
        alertViewController.alertDelegate = self
        self.present(alertViewController, animated: true, completion: nil)
    }
    
        
        // Do any additional setup after loading the view.
   
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
         let     destViewController = segue.destination as! CommentsViewController
         
         destViewController.address = addressTextField.text ?? "no address"
    }
    
}

