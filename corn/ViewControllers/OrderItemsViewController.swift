//
//  OrderItemsViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 27/6/21.
//

import UIKit

class OrderItemsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Christina's Corn"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderItems.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row   == orderItems.count {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartTotalCell") as! CartTotalTableViewCell
            var grandTotal:Double = 0.00
            //let aValues = filteredArray.compactMap { $0["a"] }
            
                
       
                for price in  orderItems{
                
                    let itemPrice = price["charge"]
                    guard let numStr = price["charge"] as? String else {
                        return cell
                    }
                    let a = Double(numStr);
                    grandTotal =  grandTotal  + a!
                }
            
            cell.totalPrice.text  = String(grandTotal)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartItemCell") as! CartItemTableViewCell
            if orderItems[indexPath.row]["product_id"] as! Int == 1 {
                cell.itamImageView.image = UIImage(named: "RCorn")
            }
            else{
                cell.itamImageView.image = UIImage(named: "BCorn")
            }
            cell.itemDescriptionlbl.text = (orderItems[indexPath.row]["name"] as! String)
            let price = orderItems[indexPath.row]["charge"]
            cell.priceLbl.text = String(format: "%f", price as! CVarArg)
            let qnt = orderItems[indexPath.row]["quantity"]
            cell.quantityLbl.text = String(format: "%d",qnt as! CVarArg)
            cell.comments.text = (orderItems[indexPath.row]["comments"] as! String)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
  
    var orderItems = [[String:Any]]()
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " Order Items"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(UINib(nibName: "CartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "cartItemCell")
        tableView.register(UINib(nibName: "CartTotalTableViewCell", bundle: nil), forCellReuseIdentifier: "cartTotalCell")
        orderItems = [ [
                 
                   "id": 1,
                   "name": "2 x Ψητό Καλαμπόκι",
                   "quantity": 2,
                   "charge": 4.4,
                   "comments": "Όχι πολύ ψημένο",
                   "product_id": 1,
                   "order_id": 100000,
                   "created_at": "2021-06-16 15:16:14",
                   "updated_at": "2021-06-16 15:16:14",
                   "deleted_at": 1,
                   "created_by": 7,
                   "updated_by": 7,
                   "deleted_by": 1
                 ],
                 [
                   "id": 2,
                   "name": "5 x Βραστό Καλαμπόκι",
                   "quantity": 5,
                   "charge": 10,
                   "comments": "Λίγο αλάτι",
                   "product_id": 2,
                   "order_id": 100000,
                   "created_at": "2021-06-16 15:16:14",
                   "updated_at": "2021-06-16 15:16:14",
                   "deleted_at": 1,
                   "created_by": 7,
                   "updated_by": 7,
                   "deleted_by": 1]]
        // Do any additional setup after loading the view.
    }
    
    @IBAction func repeatOrder(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
