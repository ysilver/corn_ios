//
//  MobileNoViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 9/6/21.
//

import UIKit
import SwiftSpinner
//self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
class MobileNoViewController: UIViewController, AlertReturn, RegisterDelegate {
    func registerReturn() {
        SwiftSpinner.hide(nil)
        self.navigationController?.performSegue(withIdentifier: "showCode", sender: self)
    }
    
    
    func returnOK() {
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBOutlet var mobileNumberTextFld: UITextField!
    @IBOutlet var nameTextFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer =   UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    @IBAction func nextBtnClicked(_ sender: Any) {
        if (nameTextFld.text) == "" || mobileNumberTextFld.text == ""{
            let alertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            
            alertViewController.message = "Attention"
            alertViewController.messageDetails = "username and mobile number are nesseccary"
            alertViewController.alertDelegate = self
            self.present(alertViewController, animated: true, completion: nil)
            
            
        }
        UserDefaults.standard.set(nameTextFld.text, forKey: "name")
        UserDefaults.standard.set(mobileNumberTextFld.text , forKey: "mobile")
        
        let parameters = [ "name":nameTextFld.text,
                           "phone":mobileNumberTextFld.text]
        
        SwiftSpinner.show("Connecting")
        NetworkOperations.sharedInstance.registerDelegate = self
        NetworkOperations.sharedInstance.register(parameters: parameters as [String : Any])
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
