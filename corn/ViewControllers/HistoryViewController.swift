//
//  HistoryViewController.swift
//  corn
//
//  Created by Ioannis Silvestridis on 26/6/21.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Christina's Corn"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderHistoryCell") as! OrderHistoryTableViewCell
        
        cell.orderid.text = "Order ID:\(orders[indexPath.row]["id"] ?? "" )"
        cell.status.text = "\(orders[indexPath.row]["status"] ?? "")"
        cell.price.text = "Price :\(orders[indexPath.row]["charge"] ?? "")"
        cell.address.text = "Address: \(orders[indexPath.row]["address"] ?? "")"
        cell.date.text = "\(orders[indexPath.row]["created_at"] ?? "")"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.performSegue(withIdentifier: "showOrderItems", sender: self)
     }
     
    var orders = [[String:Any]]()
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.register(UINib(nibName: "OrderHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "orderHistoryCell")
      orders = [[
            
              "id": 100000,
              "status": "pending",
              "charge": 14.4,
              "comments": "Παρακαλώ μην αργήσετε.",
              "address": "Αθήνα, Οδού 13",
              "client_id": 7,
              "created_at": "2021-06-16 15:16:14",
              "updated_at": "2021-06-16 15:16:14",
              "deleted_at":"",
              "created_by": 7,
              "updated_by": 7,
              "deleted_by": 1] as [String : Any],
                      [
                      "id": 100001,
                            "status": "pending",
                            "charge": 14.4,
                            "comments": "Παρακαλώ μην αργήσετε.",
                            "address": "Αθήνα, Οδού 13",
                            "client_id": 7,
                            "created_at": "2021-06-16 15:16:47",
                            "updated_at": "2021-06-16 15:16:47",
                            "deleted_at": "",
                            "created_by": 7,
                            "updated_by": 7,
                            "deleted_by": 1]]
            
  
                
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        self.tabBarController?.title = "Order History".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
