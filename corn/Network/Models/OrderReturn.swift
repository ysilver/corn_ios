// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let orderReturn = try? newJSONDecoder().decode(OrderReturn.self, from: jsonData)

import Foundation

// MARK: - OrderReturn
struct OrderReturn: Codable {
    let id: Int
    let status: String
    let charge: Double
    let comments: String?
    let address: String
    let clientID: Int
    let createdAt, updatedAt: String
    let deletedAt: String?
    let createdBy, updatedBy: Int
    let deletedBy: String?
    let client: Client
    let orderItems: [OrderItem]

    enum CodingKeys: String, CodingKey {
        case id, status, charge, comments, address
        case clientID = "client_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
        case client
        case orderItems = "order_items"
    }
}

// MARK: - Client
struct Client: Codable {
    let id: Int
    let name, userType: String
    let username, email: String?
    let phone, phoneVerifiedAt: String
    let emailVerifiedAt: String?
    let createdAt, updatedAt: String
    let deletedAt, createdBy, updatedBy, deletedBy: Int?

    enum CodingKeys: String, CodingKey {
        case id, name
        case userType = "user_type"
        case username, email, phone
        case phoneVerifiedAt = "phone_verified_at"
        case emailVerifiedAt = "email_verified_at"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
    }
}

// MARK: - OrderItem


    enum CodingKeys: String, CodingKey {
        case id, name, quantity, charge, comments
        case productID = "product_id"
        case orderID = "order_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
    }


// MARK: - Encode/decode helpers

//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public func hash(into hasher: inout Hasher) {
//        // No-op
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
//
