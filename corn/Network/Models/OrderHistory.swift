//
//  OrderHistory.swift
//  corn
//
//  Created by Ioannis Silvestridis on 26/6/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let orderHistory = try? newJSONDecoder().decode(OrderHistory.self, from: jsonData)

import Foundation

// MARK: - OrderHistory
struct OrderHistory: Codable {
    let data: [Datum]
    let links: Links
    let meta: Meta
}

// MARK: - Datum
struct Datum: Codable {
    let id: Int
    let status: String
    let charge: Double
    let comments, address: String
    let clientID: Int
    let createdAt, updatedAt: String
    let deletedAt: String?
    let createdBy, updatedBy: Int
    let deletedBy: Int?
    let orderItems: [OrderItem]

    enum CodingKeys: String, CodingKey {
        case id, status, charge, comments, address
        case clientID = "client_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
        case orderItems = "order_items"
    }
}

// MARK: - OrderItem
struct OrderItem: Codable {
    let id: Int
    let name: String
    let quantity: Int
    let charge: Double
    let comments: String
    let productID, orderID: Int
    let createdAt, updatedAt: String
    let deletedAt: String?
    let createdBy, updatedBy: Int
    let deletedBy: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, quantity, charge, comments
        case productID = "product_id"
        case orderID = "order_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
    }
}

// MARK: - Links
struct Links: Codable {
    let first, last: String
    let prev, next: Int?
}

// MARK: - Meta
struct Meta: Codable {
    let currentPage, from, lastPage: Int
    let links: [Link]
    let path: String
    let perPage, to, total: Int

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case from
        case lastPage = "last_page"
        case links, path
        case perPage = "per_page"
        case to, total
    }
}

// MARK: - Link
struct Link: Codable {
    let url: String?
    let label: String
    let active: Bool
}

// MARK: - Encode/decode helpers

