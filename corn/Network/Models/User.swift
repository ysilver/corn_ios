//
//  User.swift
//  corn
//
//  Created by Ioannis Silvestridis on 22/6/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let user = try? newJSONDecoder().decode(User.self, from: jsonData)

import Foundation

// MARK: - User
struct User: Codable {
    let id: Int
    let name, userType: String
    let username, email: String?
    let phone, phoneVerifiedAt: String
    let emailVerifiedAt: String?
    let apiToken, createdAt, updatedAt: String
    let deletedAt, createdBy, updatedBy, deletedBy: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case userType = "user_type"
        case username, email, phone
        case phoneVerifiedAt = "phone_verified_at"
        case emailVerifiedAt = "email_verified_at"
        case apiToken = "api_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case deletedBy = "deleted_by"
    }
}

// MARK: - Encode/decode helpers

