//
//  NetworkOperations.swift
//  SmartCity
//
//  Created by Ioannis Silvestridis on 1/2/21.
//

import Foundation
import Alamofire
import SwiftSpinner
//import Localize_Swift

private let sharedNetworkLayer = NetworkOperations()

protocol ReturnOrderDelegate    {
    
    func returnOrder()
}
protocol RegisterDelegate{

    func registerReturn()
}
protocol ReturnVerifyPhone  {
    func returnVerifyPhone()
}


class NetworkOperations  :NSObject{
    class var sharedInstance: NetworkOperations {
       
           return sharedNetworkLayer
       }
    var registerDelegate : RegisterDelegate? = nil
    var returnDelegate: ReturnOrderDelegate? = nil
    var verifyPhoneDelegate: ReturnVerifyPhone? = nil
//    //var issuesDelegate: ReturnRequestIssues? = nil
//    var postsDelegate: ReturnPostRequest? = nil
//    var loginDelegate:ReturnLogin? =  nil
//    var requestsDelegate:ReturnRequests?
   

    
    func placeOrder(parameters:[String:Any]){
        let baseURL = "https://corn-delivery.eu/api/orders?api_token=\(UserDefaults.standard.value(forKey:"api_token") ?? "no_token")"
                
      
                AF.request(baseURL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
                    debugPrint(response)


                    SwiftSpinner.hide(nil)

                        if let jsonData = response.data {
                            //let decoder = JSONDecoder()
                            
                            do {
                                // make sure this JSON is in the format we expect
                                if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
                                    // try to read out a string array
                                    if let message = json["message"] as? String {
                                        PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Attention", message: message)
                                    }
                                }
                            } catch let error as NSError {
                                print("Failed to load: \(error.localizedDescription)")
                            }
                            do {
                                let decoder = JSONDecoder()
                                let orderReturn = try decoder.decode(OrderReturn.self, from: jsonData)
                                DataObject.sharedInstance.orderReturn = orderReturn
        //                        self.getUserId(screenName: userObject.username )
                                print(jsonData)
                                self.returnDelegate?.returnOrder()
        //
        //                        DataObject.sharedInstance.requestTypes = model
        //                        self.typesDelegate?.returnTypes(types: model)
                               // SwiftSpinner.hide()
                            } catch DecodingError.dataCorrupted(let context) {
                                print(context)
                            } catch DecodingError.keyNotFound(let key, let context) {
                                print("Key '\(key)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch DecodingError.valueNotFound(let value, let context) {
                                print("Value '\(value)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch DecodingError.typeMismatch(let type, let context) {
                                print("Type '\(type)' mismatch:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                            } catch {
                                print("error: ", error)
                            }






                }
                    else{
                     //   SwiftSpinner.hide()
                        print("Network Error: \(String(describing: response.error))")
                        PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Connectivity issue", message: "The Internet connection appears to be offline.")

                        // self.helloDelegate
                    }
                }

            }
    
    func register(parameters:[String:Any]){

                            
//        let username = ""
//        let password = ""
//
//        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
//        let base64Credentials = credentialData.base64EncodedString()
//
//        let headers:HTTPHeaders = ["Authorization": "Basic \(base64Credentials)"]
        let baseURL = "https://corn-delivery.eu/api/clients/register";
       
        AF.request(baseURL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)
              self.registerDelegate?.registerReturn()
                           // self.postsDelegate?.returnPostRequest()
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "doCloseWindow"), object: nil)
                            
//
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didAddTree"), object: nil)

                          //  self.addTreeDelegate?.returnAddTree()
                            break
                        case .failure(let error):
                            PopUp.sharedInstance.showSimpleAlertPopUpWith(title:"Attention", message:"Something went wrong with the connection to the server")
                            print(error)
                        }
        }
    }
        func verifyPhone(parameters:[String:Any]){

                                
    //        let username = ""
    //        let password = ""
    //
    //        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
    //        let base64Credentials = credentialData.base64EncodedString()
    //
    //        let headers:HTTPHeaders = ["Authorization": "Basic \(base64Credentials)"]
            let baseURL = "https://corn-delivery.eu/api/clients/verify/\(UserDefaults.standard.value(forKey: "mobile") ?? "")";
            
            AF.request(baseURL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
              switch response.result {
                            case .success:
                  SwiftSpinner.hide(nil)
                                print(response)
                  if let jsonData = response.data {
                      //let decoder = JSONDecoder()
                      do {
                          // make sure this JSON is in the format we expect
                          if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
                              // try to read out a string array
                              if let message = json["message"] as? String {
                                  PopUp.sharedInstance.showSimpleAlertPopUpWith(title: "Attention", message: message)
                              }
                          }
                      } catch let error as NSError {
                          print("Failed to load: \(error.localizedDescription)")
                      }
                      
                      do {
                          let decoder = JSONDecoder()
                          let user = try decoder.decode(User.self, from: jsonData)
                          DataObject.sharedInstance.user = user
  //                        self.getUserId(screenName: userObject.username )
                          print(jsonData)
                          self.verifyPhoneDelegate?.returnVerifyPhone()
  //
  //                        DataObject.sharedInstance.requestTypes = model
  //                        self.typesDelegate?.returnTypes(types: model)
                         // SwiftSpinner.hide()
                      } catch DecodingError.dataCorrupted(let context) {
                          print(context)
                      } catch DecodingError.keyNotFound(let key, let context) {
                          print("Key '\(key)' not found:", context.debugDescription)
                          print("codingPath:", context.codingPath)
                      } catch DecodingError.valueNotFound(let value, let context) {
                          print("Value '\(value)' not found:", context.debugDescription)
                          print("codingPath:", context.codingPath)
                      } catch DecodingError.typeMismatch(let type, let context) {
                          print("Type '\(type)' mismatch:", context.debugDescription)
                          print("codingPath:", context.codingPath)
                      } catch {
                          print("error: ", error)
                      }


                  
                  }
                                break
                            case .failure(let error):
                                PopUp.sharedInstance.showSimpleAlertPopUpWith(title:"Attention", message:"Something went wrong with the connection to the server: \(error)")
                                print(error)
                            }
                }
        }
    
}

    


