//
//  CartTotalTableViewCell.swift
//  corn
//
//  Created by Ioannis Silvestridis on 11/6/21.
//

import UIKit

class CartTotalTableViewCell: UITableViewCell {
    @IBOutlet var totalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
