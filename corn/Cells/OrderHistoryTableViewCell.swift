//
//  OrderHistoryTableViewCell.swift
//  corn
//
//  Created by Ioannis Silvestridis on 27/6/21.
//

import UIKit

class OrderHistoryTableViewCell: UITableViewCell {
    @IBOutlet var orderid: UILabel!
    @IBOutlet var status: UILabel!
    
    @IBOutlet var date: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var address: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
