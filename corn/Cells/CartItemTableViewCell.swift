//
//  CartItemTableViewCell.swift
//  corn
//
//  Created by Ioannis Silvestridis on 11/6/21.
//

import UIKit

class CartItemTableViewCell: UITableViewCell {

    @IBOutlet var itamImageView: UIImageView!
    @IBOutlet var itemDescriptionlbl: UILabel!
    
    @IBOutlet var comments: UITextField!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        comments.isUserInteractionEnabled =  false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
