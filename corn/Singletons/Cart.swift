//
//  Cart.swift
//  corn
//
//  Created by Ioannis Silvestridis on 11/6/21.
//
private let sharedCart = Cart()

class Cart{

    var cartItem: CartItem?
    var items:[CartItem]?
    class var sharedInstance: Cart {
        
        return sharedCart
        
    }
}
