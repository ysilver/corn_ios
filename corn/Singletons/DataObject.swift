//
//  DataObject.swift
//  corn
//
//  Created by Ioannis Silvestridis on 19/6/21.
//

private let sharedDataObject = DataObject()

class DataObject{
    var orderReturn: OrderReturn?
    var user: User?
    class var sharedInstance: DataObject {
        
        return sharedDataObject
        
    }
}
