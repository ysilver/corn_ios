//
//  DummyViewController.h
//  corn
//
//  Created by Ioannis Silvestridis on 17/6/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DummyViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
